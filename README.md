Since Visual Studio is a bit pricey, I suggest using QuickSharp to develop C# applications.

QuickSharp will only work on Windows machines, so be sure you have access to one.


Installing QuickSharp:
1) Go to the QuickSharp SourceForge site: http://quicksharp.sourceforge.net/
2) Click Download Now.
3) Click on the QuickSharp exe file to download(QuickSharp 202.0.0.26942.exe)
4) After you have downloaded the exe file, double click on it and go through the setup wizard.

If you are using XP or Vista, please download version 3.5 of the Microsoft .NET Framework here:
http://www.microsoft.com/en-us/download/details.aspx?id=21

To run the examples:
1) Open the .cs file(s).
2) In the Run menu, click the Play button or Run Without Debugging (Note: If you select Run, the console might run really fast)

Here is a link to our video:
https://www.youtube.com/watch?v=8n-DEZE0iS4